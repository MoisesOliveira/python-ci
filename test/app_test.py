from src.app import returnOddNumbers
import unittest

class OddNumberTest(unittest.TestCase):

    def test_it(self):
        oddNumber = returnOddNumbers(5)
        module = oddNumber % 2
        self.assertEqual(module, 0, 'rest should be zero to be a odd number')



if __name__ == '__main__':
    unittest.main()